<?php /* Template Name: About Page */ ?>
<?php get_header(); ?>
<div id="aboutHeroImage">
  <div class="container-fluid aboutOverlay">
    <div class="aboutHero">
      <h1 class="aboutHeroHeading"><span class="whiteBorder">About Us</span></h1>
    </div>
  </div>
</div>
<div class="aboutPage">
<!-- ****** MODALS ****** -->
  <!-- CONVENTIONAL PHILANTHROPY MODAL -->
  <div class="modal fade conventPhilanModal" tabindex="-1" role="dialog" aria-labelledby="conventPhilanModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <h1>Conventional Philanthropy</h1>
          <p><span class="readText">Philanthropic methods have evolved over time to address a range of needs and increase effectiveness.  Direct contributions of goods and services are effective in serving immediate needs but are not self-sustaining and frequently have unintended side effects in removing agency.  Contributions of enabling resources and training can address an important range of needs for larger scale infrastructure (e.g. hospitals) and specialized expertise (e.g. agricultural techniques) but are again not self-sustaining and can suffer some of the same issues around agency.</span>  More recent efforts aim at Impact Investing by leveraging market-based models to enable local economic growth, such as micro-lending (Grameen Bank) and patient venture capital (Acumen). This final path has the ultimate advantage of self-sufficiency but is limited by the pace with which good local entrepreneurs can be developed and impactful solutions with a high probability of successful adoption can be identified. The BeLocal Process adds value by providing a pipeline of solutions, sourced and validated by the local population, insuring high adoption and sustainable impact. This pipeline of solutions is filled by leveraging a global crowd-sourced process of reproducible innovation to challenges discovered and tested in partnership with the local population being served.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- END CONVENTIONAL PHILANTHROPY MODAL -->
  <!-- THE BELOCAL DIFFERENCE MODAL -->
  <div class="modal fade belocalDiffModal" tabindex="-1" role="dialog" aria-labelledby="belocalDiffModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <h1>The BeLocal Difference</h1>
          <p><span class="readText">BeLocal leverages four differentiated elements to achieve Impact.  The first and most important element is a focus on deep local data gathering. Listen local.  So often, well intentioned foreigners create solutions that are unworkable, cause collateral problems, or are not maintainable over time.  Effective problem solving requires deep context.</span>  The second unique element to BeLocal is a focus on everyday quality of life problems.  Large organizations and governments take on big complex problems, but routine local challenges are not systematically addressed.  The challenges themselves can be fundamental &mdash; food security, clean water, transportation etc &mdash; but the solutions are principally built and implemented through local communities.  The third unique element is the leveraging of crowd-sourced innovation to solve the everyday problems. Leverage global. Through the web, we have the opportunity to involve millions of creative minds all over the world in an effort to innovatively address these challenges.  The key is creating a process that effectively bridges the gap between deep local data and the remote decentralized crowd of innovators.  The final unique element is a focus on self-sufficiency.  The innovations produced by BeLocal will intrinsically come out of the process with proven consumer demand and documentation on how they can be implemented locally.  BeLocal will provide a pipeline of solutions to Impact Investing NGO&rsquo;s to put these innovations in the hands of local entrepreneurs to build businesses.  We will also work with local communities that seek to cooperatively produce innovations themselves,  The key is to find Impact paths that are long term self-sustaining.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- END THE BELOCAL DIFFERENCE MODAL -->
  <!-- THE BELOCAL PROCESS MODALS -->
  <!-- Discover -->
  <div class="modal fade discoverModal" tabindex="-1" role="dialog" aria-labelledby="discoverModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal ">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <h1>Discover</h1>
          <p>The Discover step is all about gathering the kind of data needed to enable relevant, effective innovation.  An innovator solving a problem at home has a tremendous amount of critical information readily available.  She knows the culture, the way things are done, the infrastructure, the economy, the supply of local labor, local materials etc.  She often has direct experience with the problem being addressed or is surrounded by people who may and can provide insight and feedback through the process.  An innovator draws on all of this knowledge to develop an effective solution.  Now imagine losing all that information but still being asked to create a relevant, effective solution.  Really hard.  So the first step in the BeLocal Process is to Discover the data needed to bridge the gap between local challenges and global innovators.</p>
          <br>
          <p>During the Discover step enough data is collected to provide a remote innovator a virtual understanding of the local culture and environment of those trying to be served as if it was her own.  This is accomplished through well understood anthropological and ethnographic methodologies enhanced by modern technology.  Video and other multimedia technologies are used to capture what is said and observed while talking to, working alongside and observing the environment of the local population.</p>
          <br>
          <p>Video is taken about all aspects of village life, from culture and environment to food production, transportation, housing, healthcare and more.  This video is annotated for additional context and searchability.  At the same time, interviews are conducted to understand villager thoughts about local challenges, cultural sensitivities, and ideas for improvement.  This primary data is supplemented with a wide range of public information on the local culture, demographics, economy, health, climate, infrastructure, material availability and tool availability.  During the later Innovate step, specific physical measurements (e.g., size of a wheel, distance from A to B, etc) may be taken in support of particular projects.  All of this data is centrally stored and organized to be easily accessible during all of the subsequent steps to support Definition, Innovation and Impact.</p>
          <br>
          <p>The discovery process is built on a foundation of trust with the villagers with a goal to transfer the agency of change to the local population,  This goal is achieved by working with NGO&rsquo;s that are deeply connected to the local villages they work with.  The initial BeLocal effort is in partnership with Dr Pat Wright and the Centre Val Bio research station in Madagascar.  Dr. Wright has worked in Madagascar for over 30 years.  Through her work with lemurs, she has made tremendous contributions to conservation.  At the same time, she has built deep trusting relationships with the 60 villages on Madagascar where the initial BeLocal Process efforts are taking place.  The trust Dr Wright and her team have built is fundamental to the success of the Discover step (and subsequent Pilot and Impact steps as well).</p>
        </div>
      </div>
    </div>
  </div>
  <!-- Define -->
  <div class="modal fade defineModal" tabindex="-1" role="dialog" aria-labelledby="defineModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal ">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <h1>Define</h1>
          <p>Currently, innovation in the developing world requires extensive travel and a significant commitment of time to remain in residence to truly understand the challenges faced by those one hopes to serve.  The BeLocal Process removes this obstacle to innovation by leveraging technology to distribute the data captured during the Discover phase to a global set of potential innovators.  This allows for significantly more people to be involved with helping solve challenges in the developing world.  The participation of these remote innovators begins with this Define step in which critical thinkers begin to identify common challenges described or observed in the data captured so far.</p>
          <br>
          <p>Traditional philanthropic processes typically focus on a pre-identified problem that they wish to address.  The distribution of mosquito netting, providing dairy cows to villages, and building wells to provide safe drinking water are all worthy goals.  However, these organizations are a solution looking for people who have a problem they can address.  The BeLocal Process drives the agency of change from those being served forward, identifying the reported or observed challenges and discovering which problems best need to be addressed.</p>
          <br>
          <p>The most challenging part of innovation is normally not the creative process itself.  The tough part is in clearly recognizing an unmet need and stating the problem in a new way that points to the novel solution.  A clear problem statement is a great way to unlock creativity.  In the second step of the BeLocal Process, Critical thinkers participating in the process define Challenges by analyzing the data and annotations already collected and piecing together obstacles to everyday life that are either reported or observed in the data.  These or other participants in the Process can then hypothesize structured Problems that can address one or more challenges.  These Problems statements are then passed on to the Innovate step for resolution.</p>
          <br>
          <p>For example, the data might illustrate people carrying lots of heavy harvested goods by hand from the fields to the town.  The annotated data might note people carrying heavy loads, the poor condition of various paths and roads, and the fact that few if any villagers are wearing shoes.  They may note there are a few wheeled carts, but that they are poorly built and in need of good wheels.  The village elders may have said their largest obstacle is in getting food back to the village at harvest time; all observed data and annotation.</p>
          <br>
          <p>The BeLocal Process participants define Challenges by tying together different parts of the data and annotation.  Based on the example data above, one Challenge may be that the roads are uneven and difficult to traverse, backed up by both the observed data and possibly complaints during interviews.  Another Challenge may be that there are insufficient modes of transportation, such as proper carts with good wheels that can easily travel over the poorly maintained roads.  Another Challenge may be that it is difficult to carry a heavy load on unpaved roads without shoes.  The difficulty in staying parasite free when walking around without shoes may be another Challenge.  Four different observed challenges, some overlapping a higher-order reported Challenge that food simply takes too long to get back to town.</p>
          <br>
          <p>A Problem hypothesizes how a group of challenges can be addressed by a specific potential solution.  It does so in a formal way, referring to the defined challenges and demonstrating how each would be addressed.  It includes both global constraints placed on the process by BeLocal, such as cost limitations, local materials available, impact studies limiting certain potential conflicts.  It also can contain constraints the participant identifies as well.  It must include a detailed methodology for testing the solution, both remotely and locally piloted.  Most importantly, it must include a measure of performance to indicate the degree to which the solution meets its goals.</p>
          <br>
          <p>Continuing our example, a Problem could make the conjecture that building a better cart would address the Challenges of moving the harvest over poor roads, make it easier to transport food when barefoot, and allow more food to be moved at once .  An alternative Problem definition might contain a hypothesis proffering that building strong shoes from local material combined with parts of discarded tires will both allow the carriers and cart pullers to move much quicker over the poorly maintained roads while protecting them from the parasites one gets from walking barefoot.  Each problem definition, to be considered complete, would have to have all of the parts described above, but those ideas can come from one or more participants collaborating together.  The two example Problems each address a different combination of defined Challenges and when finished provide the methodology for testing and measuring their success, allowing them to be compared as to their relative effectiveness.</p>
        </div>
      </div>
    </div>
  </div>
  <!-- Innovate -->
  <div class="modal fade innovateModal" tabindex="-1" role="dialog" aria-labelledby="innovateModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal ">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <h1>Innovate</h1>
          <p>While much philanthropy relies on innovation to provide assistance, this step of the Process for BeLocal couldn&rsquo;t be more different from traditional organizations.  Traditional organizations either address a single topic (Save the&mldr;.) or are formed around how a single innovation can serve one or more populations (Let&rsquo;s give everyone ...).  At this stage in the BeLocal Process, we have carefully built up a collection of well defined Problems, all of which identify exactly which Challenges they address and how, each of which has been documented and sourced from those being served.  This insures that any solutions to these problems will directly address the observed and reported obstacles in the lives of those being served.</p>
          <br>
          <p>It is now, that participants in the BeLocal Process get the opportunity to innovate solutions to these problems.  For a solution to be viable it must provide detailed methodology for implementation, both for testing, such as building a prototype and for sustainable production.  It must meet the listed constraints, some of which are imposed by BeLocal, such as ethical concerns or sustainability issues.  Others may be more specific to the locale. For instance, in a village near a jungle at risk, any solution that requires burning wood and whose very success would likely result in deforestation would not pass muster.  Some constraints may simply be the cost under which a pilot project can be performed.</p>
          <br>
          <p>Once these issues are addressed, the innovation is ready for selection for testing in a pilot project.</p>
        </div>
      </div>
    </div>
  </div>
  <!-- Pilot -->
  <div class="modal fade pilotModal" tabindex="-1" role="dialog" aria-labelledby="pilotModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal ">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <h1>Pilot</h1>
          <p>The BeLocal Process is designed to accelerate the process of innovation through crowd sourced innovation.  At this phase of the process, having done so, there is a pipeline of solutions all of which are potential candidates for testing.  As our stated goal is to empower people to make their own lives better, those being served are welcome to participate in any phase they are able.  However, the Process truly shifts the agency of change by insuring their participation in the three phases of Discover, Pilot, and Impact.  In the Pilot step, the participation of those being served provides inexpensive validation as to the performance of solutions.  The local feedback is crucial to evaluate the likely adoption of such a solution if implemented.  Furthermore, the performance data resulting from the pilot can help predict the impact on investment (ioi) that delivering such a solution may have. </p>
          <br>
          <p>The selection of which solutions are piloted is a balance between prioritization based on local feedback, design, projected impact, funding, and practicality.  While all the solutions may not get piloted, those that do already have a much better chance of success than the conjecture that is at the heart of traditional philanthropic organizations, that the solution they are proposing will work and be adopted by those they are trying to serve.</p>
          <br>
          <p>For example, in Madagascar we have been told, much of the population is provided with mosquito netting to help prevent the disease and parasites that mosquitoes spread.  However, mosquito netting is the strongest and finest netting that many of the villages ever own.  As such, despite being covered in poisons intended for the mosquitos they are typically employed as either fishing nets or to shade the crops.  This is an example of a valid solution deployed without proper agency.  The solutions coming out of the BeLocal Process have been sourced by the people and are piloted by the people. By piloting the  adoption of a solution, such as the mosquito netting as fishing nets or shades, the BeLocal Pilot stage would quickly identify solutions that are not yet ready for delivery and do so at a greatly reduced cost.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Impact -->
  <div class="modal fade impactModal" tabindex="-1" role="dialog" aria-labelledby="impactModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal ">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <h1>Impact</h1>
          <p>Having identified the solutions that are ready for adoption, the Impact stage is where BeLocal can begin to help people truly make their lives better.  BeLocal identifies, wherever possible, local individuals and organizations that can deliver these solutions.  BeLocal identifies, and partners with, organizations capable of providing proper support and expertise to these local participants.  This support can come in many forms from investment capital, to business acumen, from training to service trips,. Whatever the form of support, the goal is to deliver the solution in a self-sustaining form, to insure its ongoing success and to, when possible, get synergistic effects that improves lives across the community.  The delivery of a solution must include a plan for recording the impact of the solution and the lessons learned.  By doing so, the knowledge gained by the deployment of a solution can be leveraged in all steps of the BeLocal Process, improving the quality and rate of innovation for future solutions.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- END THE BELOCAL PROCESS MODALS -->
  <!-- ABOUT THE TEAM MODALS -->
  <!-- Jeff Nagel -->
  <div class="modal fade jeffNagelModal" tabindex="-1" role="dialog" aria-labelledby="jeffNagelModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <img src="<?php bloginfo('template_url'); ?>/img/jeffNagel.jpg" alt="Jeff Nagel">
          <h1>Jeff Nagel</h1>
          <p>Jeff joined AEA in 2015 as an Operating Partner. He is primarily focused on working with portfolio companies to maximize value creation through improved operating performance and cross-portfolio best practice development. He also works with AEA’s Risk Committee on the implementation of its ESG initiative. Jeff currently serves on the board of Generation Brands, LoneStar, Pro Mach and Sparrows and TricorBraun.</p>
          <p>Before joining AEA, Jeff was the chief executive officer of NBTY Inc., a portfolio company of The Carlyle Group. NBTY is a &dollar;3B global manufacturer and marketer of supplements. He was formerly a corporate officer of GE and most recently led the Global Services division of GE Oil &amp; Gas as VP and GM, based out of Italy. He previously ran GE Inspection Technologies as president and chief executive officer, based out of Germany. Prior to that, he was the president of GE Home Electric Products. He also served as general manager of business development in GE Aircraft Engines. He joined GE in May of 1997 as the manager of business development at GE Lighting. Before joining GE, Jeff was vice president of marketing at a biotech start-up, Energy Biosystems Corporation. He started his career in management consulting with Strategic Planning Associates (now Mercer Management) and Cannon Associates. He graduated from Carnegie Mellon University in 1987 with an MBA and BS in Mechanical Engineering, both with Honors.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Mickie Nagel -->
  <div class="modal fade mickieNagelModal" tabindex="-1" role="dialog" aria-labelledby="mickieNagelModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <img src="<?php bloginfo('template_url'); ?>/img/mickieNagel.jpg" alt="Mickie Nagel">
          <h1>Mickie Nagel</h1>
          <p>Mickie Nagel has over 25 years of experience in advertising, marketing, public relations, and community development.  She spent the first half of her career in marketing for super-regional shopping malls where she was responsible for developing strategic marketing plans, advertising campaigns, public relations and special events.  As part of her work, she designed and conducted events that delivered up to 100,000 people.  Her campaigns won multiple industry awards for creative excellence.  She subsequently worked for Hewlett Packard (Compaq Computer) running large scale B2B marketing events.  The second half of her career has been spent raising four children while living in Europe for seven years.  During this period, she travelled extensively and took leadership roles in charitable works and community events.  Currently, she continues her work in marketing, advertising and special events for both business and not-for-profit companies.   She has a Bachelor’s Degree in Business with a Marketing Major from Davenport University (DCB).
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Eric Bergerson -->
  <div class="modal fade ericBergersonModal" tabindex="-1" role="dialog" aria-labelledby="ericBergersonModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <img src="<?php bloginfo('template_url'); ?>/img/ericBergerson.jpg" alt="Eric Bergerson">
          <h1>Eric Bergerson</h1>
          <p>Eric is the Director of Research at TickerTags.com, a financial technology company focused on discovering market insights before they become news by monitoring the social web in real-time, and aligning the conversations in social media with investable assets.  Before joining TickerTags, he was a senior engineering director at Vendavo responsible for the analytic team leveraging big data technology to provide actionable insights to enable business to sell more profitably.  Eric is a hard-core coder and an experienced manager, applying his experience in software development, machine learning and data science to problems for Financial Services, Healthcare, Entertainment and Market Research.  He has been a founder in three companies and holds both a B.S.E.E. from Carnegie Mellon University (1986) and an M.S.E.E. from the University of Rhode Island (1988).
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Patricia Wright -->
  <div class="modal fade patriciaWrightModal" tabindex="-1" role="dialog" aria-labelledby="patriciaWrightModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <img src="<?php bloginfo('template_url'); ?>/img/patriciaWright.jpg" alt="Patricia Wright">
          <h1>Patricia Wright</h1>
          <p>Dr. Wright is a Distinguished Service Professor in the Department of Anthropology at the State University of New York at Stony Brook. Dr. Wright has served as the Executive Director for the Institute for the Conservation of Tropical Environments (ICTE) since 1992 and founded the Centre ValBio Research station in Madagascar in 2002. She has studied behavioral ecology of non-human primates in South America, Asia and Madagascar. Her research interests include primate behavior and ecology, female dominance, male parenting, the evolution of tropical biodiversity, biodiversity conservation, climate change in the tropics and conservation genetics.  Patricia is a Fellow of the John D. and Catherine T. MacArthur Foundation, the American Association for the Advancement of Sciences and has won numerous awards and medals including the “Nobel Prize” in conservation, the Indianapolis Prize for Animal Conservation.  She has authored or co-authored many books including her own two volume autobiography and published over a hundred and fifty scientific papers.  Her work has been featured in documentaries and TV shows, most recently in CNN’s “Parts Unknown” and ABC Nightline news.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Jesse McKinney -->
  <div class="modal fade jesseMckinneyModal" tabindex="-1" role="dialog" aria-labelledby="jesseMckinneyModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <img src="<?php bloginfo('template_url'); ?>/img/jesseMckinney.jpg" alt="Jesse McKinney">
          <h1>Jesse McKinney</h1>
          <p>Jesse McKinney, a trained designer, is currently serving as the Chief Technology Officer of Centre ValBio. He has for the last five years lived and worked in rural Madagascar advising and implementing technology forward initiatives for health and scientific research. Prior to his current position, Jesse served in the United States Peace Corps Madagascar as an Internet and Communication Technology Volunteer. Before working in Madagascar, Jesse had a professional career as an Industrial and Digital Designer, where he held positions at a number of engineering facilities and design firms throughout the USA. In addition to his experience as a designer, Jesse has significant professional experience in robotic systems integration, movement and perception centric industrial automation engineering, and software development. Jesse’s current research interest is in novel applications of low cost distributed sensor networks for ecosystems scale data capture and processing. Jesse holds a BS in Digital Design from the College of DAAP at the University of Cincinnati.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Peter Small -->
  <div class="modal fade peterSmallModal" tabindex="-1" role="dialog" aria-labelledby="peterSmallModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <img src="<?php bloginfo('template_url'); ?>/img/peterSmall.jpg" alt="Peter Small">
          <h1>Peter Small</h1>
          <p>Dr. Small is the Founding Director of the Stony Brook University Global Health Institute, a highly collaborative, transdisciplinary campus based program that includes teaching, research and service with a mission to reduce poverty, ecological devastation and disease in Madagascar and other poor countries.
          Immediately prior to joining Stony Brook, Peter worked for the Bill and Melinda Gates Foundation for nearly 13 years where he was responsible for developing the foundation’s tuberculosis strategy, building the programs core partnerships and country programs, hiring and managing the TB team, overseeing the vaccine, drug and diagnostic product development activities and serving as the foundation’s voice for tuberculosis. Peter completed his postgraduate training in internal medicine at UCSF and infectious diseases at Stanford University before spending about a decade on the faculty of Stanford’s Division of Infectious Disease and Geographic Medicine. Peter has published more than 150 articles including molecular epidemiologic studies that helped to shape the public health response to the resurgence of tuberculosis in the USA in the 1990’s and seminal papers on the origin, nature and consequence of genetic variability within the species M. tuberculosis. He is a member of several honor societies such as Alpha Omega Alpha Honor Medical Society, American Society for Clinical Investigation, was awarded the Distinguished Alumnus Award from his alma mater, the University of Florida and is a fellow in the American Academy of Microbiology He is a recipient of the Princess Chichibu Global TB Award for his pioneering contributions to global tuberculosis control.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Petar Djuric -->
  <div class="modal fade petarDjuricModal" tabindex="-1" role="dialog" aria-labelledby="petarDjuricModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <img src="<?php bloginfo('template_url'); ?>/img/petarDjuric.jpg" alt="Petar Djuric">
          <h1>Petar Djuric</h1>
          <p>Petar M. Djurić received a B.S. and M.S. degrees in electrical engineering from the University of Belgrade, Belgrade, Yugoslavia, respectively, and a Ph.D. degree in electrical engineering from the University of Rhode Island, Kingston, RI, USA. He is currently a Professor and Chair of the Department of Electrical and Computer Engineering, Stony Brook University, Stony Brook, NY, USA. His research has been in the area of signal and information processing with primary interests in the theory of signal modeling, detection, and estimation; Monte Carlo-based methods; signal and information processing over networks; machine learning, RFID and the IoT. He has been invited to lecture at many universities in the United States and overseas. Prof. Djurić was a recipient of the IEEE Signal Processing Magazine Best Paper Award in 2007 and the EURASIP Technical Achievement Award in 2012. In 2008, he was the Chair of Excellence of Universidad Carlos III de Madrid-Banco de Santander. From 2008 to 2009, he was a Distinguished Lecturer of the IEEE Signal Processing Society. He has been on numerous committees of the IEEE Signal Processing Society and of many professional conferences and workshops. He is the Editor-in-Chief of the IEEE Transactions on Signal and Information Processing over Networks. Prof. Djurić is a Fellow of IEEE and EURASIP.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Rob Kukta -->
  <div class="modal fade robKuktaModal" tabindex="-1" role="dialog" aria-labelledby="robKuktaModal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content about-offwhite-bg-modal">
        <div class="modal-body text-center">
          <span data-dismiss="modal" aria-label="Close" aria-hidden="true"><i class="fa fa-times"></i></span>
          <img src="<?php bloginfo('template_url'); ?>/img/robKukta.jpg" alt="Rob Kukta">
          <h1>Rob Kukta</h1>
          <p>Robert Kukta is the Associate Dean for Undergraduate Programs in the College of Engineering and Applied Sciences at Stony Brook University.  He joined Stony Brook in 1999 as a faculty member of the Department of Mechanical Engineering, after serving as a Postdoctoral Scholar of Applied Mechanics at Caltech.  He earned his PhD in Engineering (1998) from Brown University. His research is in solid mechanics, involving the mathematical modeling and simulation of microstructural evolution.  He is best known for his work on the dynamics of dislocations and the mechanics of steps on vicinal surfaces of crystals.  He received an NSF CAREER award for this work.  Recently, Robert’s focus has shifted towards education and diversity.  Prior to becoming Associate Dean, he successively served as Graduate Program Director and then Undergraduate Program Director for the Department of Mechanical Engineering.  In Mechanical Engineering, he advanced successful initiatives to grow the Master’s program, improve undergraduate academic advising, and increase graduation rates.  He is currently working on College initiatives on experiential learning, engineering innovation and entrepreneurship, industry-based senior design projects, broadening participation of women in engineering, and global engineering with a new engineering summer field school in off-grid Africa.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- END ABOUT THE TEAM MODALS -->
<!-- ****** END MODALS ****** -->
  <div style="border-bottom: 3px solid #1371d6;" class="about-offwhite-bg mediumTopPadding mediumBottomPadding container text-center">
    <h3 class="aboutSubText">We believe that great innovation starts with empathy for the people we serve.  Our process begins and ends with their input because our success is measured by their voluntary adoption of innovations that <span class="blue">improve their lives</span>.  We believe that agency matters and makes for the best long term changes.  BeLocal is ultimately about empowering people to make their own lives better.</h3>
    <!--<span class="btn" data-toggle="modal" data-target=".programDevModal">See More</span>-->
  </div>
  <div class="about-offwhite-bg mediumTopPadding mediumBottomPadding container text-center">
    <h1 class="aboutMainHeading">BeLocal Partnership</h1>
    <h3 class="aboutSubText">The BeLocal Group was formed at the beginning of 2017 in partnership with <span class="inline-modal green" data-toggle="modal" data-target=".patriciaWrightModal">Dr Patricia Wright</span> of Stony Brook University (SBU) and <span class="inline-modal green" data-toggle="modal" data-target=".jesseMckinneyModal">Jesse McKinney</span> of Centre ValBio.  We held our introductory kick-off session at Stony Brook in February of 2017 with a diverse interdisciplinary group of professors and students.  From that session, our advisory group was created and a plan put in place to launch the BeLocal Process with the SBU Senior Engineering Design classes in the Fall of 2018.  Our first major expedition is underway on Madagascar, to be completed in July.  The first Discover data set is being developed in parallel with the BeLocal Process web platform.  The process, platform, and suggested project areas will be presented to the Senior Design professors in August and the students in September.  The best projects will be selected for piloting on Madagascar in June of 2018.  Stay tuned!</h3>
  </div>
  <div style="border-top: 3px solid #1371d6;" class="aboutTheTeam container text-center">
    <h1 class="aboutMainHeading">Meet The Team</h1>
    <h3 class="aboutSubText">BeLocal Group</h3>
    <div class="row">
      <div class="col-xs-4"><img src="<?php bloginfo('template_url'); ?>/img/jeffNagel.jpg" alt="Jeff Nagel" data-toggle="modal" data-target=".jeffNagelModal"><div class="caption inline-modal " data-toggle="modal" data-target=".jeffNagelModal">Jeff Nagel</div></div>
      <div class="col-xs-4"><img src="<?php bloginfo('template_url'); ?>/img/mickieNagel.jpg" alt="Mickie Nagel" data-toggle="modal" data-target=".mickieNagelModal"><div class="caption inline-modal " data-toggle="modal" data-target=".mickieNagelModal">Mickie Nagel</div></div>
      <div class="col-xs-4"><img src="<?php bloginfo('template_url'); ?>/img/ericBergerson.jpg" alt="Eric Bergerson" data-toggle="modal" data-target=".ericBergersonModal"><div class="caption inline-modal " data-toggle="modal" data-target=".ericBergersonModal">Eric Bergerson</div></div>
    </div>
      <h3 class="aboutSubText">Stony Brook University</h3>
      <div class="row">
        <div class="col-xs-6"><img src="<?php bloginfo('template_url'); ?>/img/patriciaWright.jpg" alt="Patricia Wright" data-toggle="modal" data-target=".patriciaWrightModal"><div class="caption inline-modal " data-toggle="modal" data-target=".patriciaWrightModal">Patricia Wright</div></div>
        <div class="col-xs-6"><img src="<?php bloginfo('template_url'); ?>/img/jesseMckinney.jpg" alt="Jesse McKinney" data-toggle="modal" data-target=".jesseMckinneyModal"><div class="caption inline-modal " data-toggle="modal" data-target=".jesseMckinneyModal">Jesse McKinney</div></div>
      </div>
      <h3 class="aboutSubText">Advisory Council</h3>
      <div class="row">
        <div class="col-xs-4"><img src="<?php bloginfo('template_url'); ?>/img/peterSmall.jpg" alt="Peter Small" data-toggle="modal" data-target=".peterSmallModal"><div class="caption inline-modal " data-toggle="modal" data-target=".peterSmallModal">Peter Small</div></div>
        <div class="col-xs-4"><img src="<?php bloginfo('template_url'); ?>/img/petarDjuric.jpg" alt="Petar Djuric" data-toggle="modal" data-target=".petarDjuricModal"><div class="caption inline-modal " data-toggle="modal" data-target=".petarDjuricModal">Petar Djuric</div></div>
        <div class="col-xs-4"><img src="<?php bloginfo('template_url'); ?>/img/robKukta.jpg" alt="Rob Kukta" data-toggle="modal" data-target=".robKuktaModal"><div class="caption inline-modal " data-toggle="modal" data-target=".robKuktaModal">Rob Kukta</div></div>
      </div>
      <h3 class="aboutSubText">Contributors</h3>
      <div class="row">
        <div class="col-sm-12"><div class="caption"><a href="http://tjsalgado.com" target="_blank">TJ Salgado &mdash; Web Design</a></div></div>
      </div>
  </div>
</div><!--./aboutPage -->
<?php get_footer(); ?>
